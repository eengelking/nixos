{ config, pkgs, ... }:

let
## User Specific
  user = "eengelking";
  initialPassword = "password";
  userDescription = "Ed Engelking";
  userAddress = "ed@engelking.me";
  authKeys = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDKCRt474nPc0b21MOBYN9066TOq4D+Fip62hdTl1ArPo2kS1kczNsF7qc7aFh3yBnhmJqvc3mwslMAr+NFfiS/WNNaoglgyYLaC6pkCH8MttHnljuq8zRl6vuDOTTOa1nfiN7yzYiIpGK50rFow8l+y8juAmKcAzt6c7xHRxQjfvngrhIRBU8XJpUeAyjaRKBWNvtn6l4zgft8v/XHOuy5FYX3BCgoFQiXCLn2LlLE+55/C8Z7WtWfLBoPuBF8OF+5L23peFLKL2iwpdtTHs7yCCaB6T2L3oHoTvQNKUjKdt3T+AxBrE8RgyyuZ6AzyYzMiB1tZtABUw59R0UpqjNUqhq+e+V8KY9EAUezIwpPljD3yyssONFY9HFO9vkTLU4tIbdXAsXLCFHuFEhmtfGwGHBJ1UIHxqD7kuvE79OVPsnrPiIhs7EdO7awjuOWudqnidMwH3QvWRdkvNyM3L0fbU5V+Wt6ADR7HRhnZ+yL+QoJDQ4BiNk7WceVfWg/Xufd9tXdgeEzjtmhnPS271CS/eTjTBif5G7WaeT3XH8zkOlcIqdTBNPNezTbiuC20Gxsbanuol6Fx5cXd5FmQkIjSUL4gOSgbC44J7yi8IwxJjnACM0EMzGT5+Y7myf5STwyFW9CupL8vS+kq/yJ0AQ0QzLhrFrGWmcRa14WjiT1fQ==";
## Machine Specific
  disk = "/dev/sda";
  hostname = "devbox";
  locale = "en_US.UTF-8";
  timezone = "America/Los_Angeles";
  maxFiles = 2097152;
## NixOS Specific
  autoUpgrade = true;
  currentStateVersion = "23.05";
  currentStateChannel = "https://nixos.org/channels/nixos-23.05";
## Boot Options
  timeoutLimit = 5;
  configurationLimit = 5;
## Graphical environment
  enableDesktop = true;
  enableSound = true;
in
{
  imports =
    [
      ./hardware-configuration.nix                                        # Include hardware scan results.
      /nix/var/nix/profiles/per-user/root/channels/home-manager/nixos     # Include home-manager.
    ];

  ## BOOT PARAMETERS
  # boot = {
  #   kernelPackages = pkgs.linuxPackages_latest;                           # Latest kernel
  #   kernel.sysctl = {                                                     # Kernel sysctl settings
  #     "fs.file-max" = maxFiles;                                           # Maximum number of open files
  #   };
  #   loader = {                                                            # GRUB 2 boot loader
  #     timeout = timeoutLimit;                                                        # Boot menu timeout
  #     efi = {                                                             # Enable EFI
  #       canTouchEfiVariables = true;                                      # Enable EFI variables
  #       efiSysMountPoint = "/boot";                                       # Mount point for EFI
  #     };
  #     grub = {
  #       enable = true;                                                    # Enable GRUB
  #       version = 2;                                                      # Use GRUB 2
  #       device = disk;                                                    # Hard drive to install
  #       efiSupport = true;                                                # Enable EFI
  #       enableCryptodisk = true;                                          # Enable encrypted root
  #       enableUefiSecureBoot = true;                                      # Enable secure boot
  #       configurationLimit = configurationLimit;                                           # Limit number of grub menu entries
  #     };
  #   };
  # };

  boot = {
    loader = {
      grub = {
        enable = true;
        device = disk;
        useOSProber = true;
        enableCryptodisk = true;
      };
    };
    initrd = {
      secrets = {
        "/crypto_keyfile.bin" = null;
      };
      luks.devices."luks-711a820e-81bb-43f7-ba93-621e343f590f".keyFile = "/crypto_keyfile.bin";
    };
  };

  ## CREATE DIRECTORIES w/ PERMISSIONS
  systemd.tmpfiles.rules = [                                             # Create custom directories.
    "d /data 0755 ${user} ${user} -"
    "d /data/oh-my-zsh 0755 ${user} ${user} -"
    "d /data/oh-my-zsh/custom 0755 ${user} ${user} -"
    "d /data/oh-my-zsh/custom/themes 0755 ${user} ${user} -"
  ];

  ## NETWORKING
  networking = {
    hostName = hostname;                                                  # Set hostname.
    networkmanager.enable = true;                                         # Enable NetworkManager.
    wireless.enable = false;                                              # Disable wireless.
    useDHCP = false;                                                      # Disable DHCP.
    interfaces.eth0.useDHCP = false;                                      # Enable DHCP on eth0.
    firewall.enable = false;                                              # Disable firewall.
  };

  ## LOCALIZATION
  time.timeZone = timezone;                                               # Set timezone.
  console = {
    font = "Lat2-Terminus16";                                             # Console font.
    keyMap = "us";                                                        # Console keyboard layout.
  };
  i18n = {
    defaultLocale = locale;                                               # Default locale.
  };

  ## ONE-OFFs
  sound.enable = enableSound;                                             # Enable sound.
  hardware.pulseaudio.enable = false;                                     # Enable pulseaudio.
  virtualisation.docker.enable = true;                                    # Enable docker.
  nixpkgs.config.allowUnfree = true;                                      # Allow unfree packages.

  ## SECURITY
  security = {
    rtkit.enable = true;                                                  # Enable rtkit.
    sudo = {
      enable = true;                                                      # Enable sudo.
      wheelNeedsPassword = false;                                         # Disable sudo password.
    };
  };

  ## SERVICES
  services = {
    printing.enable = true;                                               # Enable printing.
    openssh.enable = true;                                                # Enable ssh.
    flatpak.enable = true;                                               # Enable flatpak.
    xserver = {
      enable = enableDesktop;                                             # Enable X11.
      layout = "us";                                                      # Keyboard layout.
      xkbVariant = "";                                                    # Keyboard variant.
      # libinput.enable = true;                                           # Enable touchpad, should be enabled by default.
      displayManager = {
        defaultSession = "gnome";                                         # Set default session.
        gdm = {
          enable = enableDesktop;                                         # Enable GDM.
          wayland = enableDesktop;                                        # Enable Wayland.
        };
      };
      desktopManager = {
        gnome.enable = enableDesktop;                                     # Enable GNOME.
      };
    };
    pipewire = {                                                          # Enable sound.
      enable = enableSound;                                               # Enable pipewire.
      pulse.enable = enableSound;                                         # Enable pulseaudio.
      alsa.enable = enableSound;                                          # Enable ALSA.
      alsa.support32Bit = enableSound;                                    # Enable 32-bit support.
    };
  };

  ## USERS
  users = {
    groups.${user}.gid = 1000;                                            # Set students group ID.
    users.${user} = {
      shell = pkgs.zsh;                                                   # Set shell.
      initialPassword = initialPassword;                                  # Set password.
      isNormalUser = true;                                                # Create user.
      description = userDescription;                                      # User description.
      group = user;                                                       # Default user group.
      extraGroups = [                                                     # Extra groups.
        "wheel"
        "networkmanager"
        "audio"
        "video"
        "lp"
        "docker"
        "users"
      ];
      packages = with pkgs; [
        firefox
      ];
      openssh.authorizedKeys.keys = [ authKeys ];                        # Add SSH key.
    };
  };

  ## HOME MANAGER
  home-manager = {                                                        # Home Manager configuration.
    useGlobalPkgs = true;                                                 # Use global packages.
    useUserPackages = true;                                               # Use user packages.

    users.${user} = { pkgs, ... }: {                                      # User configuration.
      home = {
        stateVersion = currentStateVersion;                                           # Version of NixOS.
        packages = with pkgs; [                                          # Install packages.
          firefox
        ];
      };

      programs.vim = {
        enable = true;                                                    # Enable vim.
        defaultEditor = true;                                             # Set vim as default editor.
        settings = {
          history = 1000;
          mouse = "v";
          tabstop = 2;
          expandtab = true;
          shiftwidth = 2;
          number = true;
          modeline = true;
        };
        extraConfig = ''
          syntax on
          set encoding=utf-8
          set showmode
          set showcmd
          set ruler
          set matchpairs+=<:>
          set formatoptions=tcqrn1
          set softtabstop=2
          set wrap
          set linebreak
          set showbreak=▹
          set scrolloff=5
          set backspace=indent,eol,start
          let &t_ti.="\<Esc>[1 q"
          let &t_SI.="\<Esc>[5 q"
          let &t_EI.="\<Esc>[1 q"
          let &t_te.="\<Esc>[0 q"
        '';
      };

      programs.git = {                                                    # Git configuration.
        enable = true;                                                    # Enable git.
        userName = "Ed Engelking";                                        # Set git username.
        userEmail = "ed@engelking.me";                                     # Set git email.
      };

      programs.zsh = {                                                    # ZSH configuration.
        enable = true;
        enableCompletion = true;                                          # Enable completion.
        enableAutosuggestions = true;                                     # Enable autosuggestions.
        enableSyntaxHighlighting = true;                                  # Enable syntax highlighting.

        shellAliases = {
          ab-reset = "unset AWS_PROFILE";
          ab-dev-east = "export AWS_PROFILE=ab-dev-east";
          ab-dev-west = "export AWS_PROFILE=ab-dev-west";
          ab-business-east = "export AWS_PROFILE=ab-business-east";
          ab-business-west = "export AWS_PROFILE=ab-business-west";
          ab-govcloud-east = "export AWS_PROFILE=ab-govcloud-east";
          ab-govcloud-west = "export AWS_PROFILE=ab-govcloud-west";
          age-test = "export SOPS_AGE_KEY_FILE=~/.age/key-test.txt SOPS_AGE_RECIPIENTS=age1qeprrxh997g4dkfp34wfh8elkt9366rvavvzjjsvkn8zqd9mqems6udfhh";
          age-dev = "export SOPS_AGE_KEY_FILE=~/.age/key-dev.txt SOPS_AGE_RECIPIENTS=age1qeprrxh997g4dkfp34wfh8elkt9366rvavvzjjsvkn8zqd9mqems6udfhh";
          age-stg = "export SOPS_AGE_KEY_FILE=~/.age/key-stg.txt SOPS_AGE_RECIPIENTS=age1nxmv3klvjt4r0ey5k8dn8jk7wrwulsa083v8pv635hnd0dkugfys5ne878";
          age-prd = "export SOPS_AGE_KEY_FILE=~/.age/key-prod.txt SOPS_AGE_RECIPIENTS=age19ugvhes2nqjgd4mvgvwfxypymgqqq4v9dka5t2jys9uza5a9sppqf5rsxx";
          age-reset = "unset SOPS_AGE_KEY_FILE";
        };

        sessionVariables = {                                              # Session variables.
          SPACESHIP_PROMPT_SYMBOL = "➜";
          SPACESHIP_PROMPT_ADD_NEWLINE = true;
          SPACESHIP_PROMPT_SEPARATE_LINE = true;
          SPACESHIP_PROMPT_PREFIXES_SHOW = true;
          SPACESHIP_PROMPT_SUFFIXES_SHOW = true;
          SPACESHIP_PROMPT_DEFAULT_PREFIX = "via ";
          SPACESHIP_PROMPT_DEFAULT_SUFFIX = " ";
          SPACESHIP_TIME_SHOW = false;
          SPACESHIP_TIME_PREFIX = "at ";
          SPACESHIP_TIME_SUFFIX = " ";
          SPACESHIP_TIME_FORMAT = false;
          SPACESHIP_TIME_12HR = false;
          SPACESHIP_TIME_COLOR = "yellow";
          SPACESHIP_HOST_SHOW = true;
          SPACESHIP_HOST_PREFIX = "at ";
          SPACESHIP_HOST_SUFFIX = " ";
          SPACESHIP_HOST_COLOR = "green";
          SPACESHIP_DIR_SHOW = true;
          SPACESHIP_DIR_PREFIX = "in ";
          SPACESHIP_DIR_SUFFIX = " ";
          SPACESHIP_DIR_TRUNC = 3;
          SPACESHIP_DIR_COLOR = "cyan";
          SPACESHIP_GIT_SHOW = true;
          SPACESHIP_GIT_PREFIX = "on ";
          SPACESHIP_GIT_SUFFIX = " ";
          SPACESHIP_GIT_SYMBOL = " ";
          SPACESHIP_GIT_BRANCH_SHOW = true;
          SPACESHIP_GIT_BRANCH_PREFIX = " ";
          SPACESHIP_GIT_BRANCH_SUFFIX = "";
          SPACESHIP_GIT_BRANCH_COLOR = "magenta";
          SPACESHIP_GIT_STATUS_SHOW = true;
          SPACESHIP_GIT_STATUS_PREFIX = " [";
          SPACESHIP_GIT_STATUS_SUFFIX = "]";
          SPACESHIP_GIT_STATUS_COLOR = "red";
          SPACESHIP_GIT_STATUS_UNTRACKED = "?";
          SPACESHIP_GIT_STATUS_ADDED = "+";
          SPACESHIP_GIT_STATUS_MODIFIED = "!";
          SPACESHIP_GIT_STATUS_RENAMED = "»";
          SPACESHIP_GIT_STATUS_DELETED = "✘";
          SPACESHIP_GIT_STATUS_STASHED = "$";
          SPACESHIP_GIT_STATUS_UNMERGED = "=";
          SPACESHIP_GIT_STATUS_AHEAD = "⇡";
          SPACESHIP_GIT_STATUS_BEHIND = "⇣";
          SPACESHIP_GIT_STATUS_DIVERGED = "⇕";
          SPACESHIP_DOCKER_SHOW = true;
          SPACESHIP_DOCKER_PREFIX = "on ";
          SPACESHIP_DOCKER_SUFFIX = " ";
          SPACESHIP_DOCKER_SYMBOL = "🐳 ";
          SPACESHIP_DOCKER_COLOR = "cyan";
        };

        oh-my-zsh = {
          enable = true;                                                  # Enable oh-my-zsh.
          custom = "/data/oh-my-zsh/custom";                              # ZSH_CUSTOM path.
          theme = "spaceship";                                            # oh-my-zsh theme.
          plugins = [                                                     # oh-my-zsh plugins.
            "git"
            "kubectl"
            "docker"
            "docker-compose"
            "ansible"
            "aws"
            "dotenv"
            "github"
            "golang"
            "history"
            "kubectx"
            "mongocli"
            "multipass"
            "nmap"
            "npm"
            "node"
            "nvm"
            "pip"
            "python"
            "ssh-agent"
            "systemd"
            "terraform"
            "vscode"
            "zsh-interactive-cd"
            "zsh-navigation-tools"
          ];
        };
      };
    };
  };

  ## PROGRAMS
  programs = {
    zsh = {
      enable = true;                                                      # Enable zsh.
      enableCompletion = true;                                            # Enable zsh completion.
      autosuggestions.enable = true;                                      # Enable zsh autosuggestions.
      syntaxHighlighting.enable = true;                                   # Enable zsh syntax highlighting.
      ohMyZsh = {
        enable = true;                                                    # Enable oh-my-zsh.
        theme = "spaceship";                                              # Set theme.
        custom = "/data/oh-my-zsh/custom";                                # ZSH_CUSTOM path.
      };
    };
  };

  ## ENVIRONMENT
  environment = {

    shellAliases = {
      k = "kubectl";
      mp = "multipass";
      docker-compose = "docker compose";
    };

    sessionVariables = rec {
      EDITOR = "vim";                                                     # Set editor.
      PAGER = "less";                                                     # Set pager.
      ZSH_CUSTOM = "/data/oh-my-zsh/custom";                              # ZSH_CUSTOM path.
    };

    shells = with pkgs; [                                                 # Install shells.
      zsh
    ];

    systemPackages = with pkgs; [                                         # Install packages.
      age
      ansible
      awscli2
      btop
      byobu
      bzip2
      cacert
      ctop
      docker
      eksctl
      ethtool
      fira-code
      git
      grype
      helmfile
      htop
      jq
      k3d
      k9s
      kind
      kubeaudit
      kubectl
      kubecfg
      kubectx
      kubernetes-helm
      mkcert
      neofetch
      nettools
      nmap
      pgcli
      procps
      screen
      sops
      terraform
      tilt
      trivy
      unzip
      vim
      wget
      yq
      zsh
      zsh-autosuggestions
      zsh-completions
    ];
  };

  ## SYSTEM SETTINGS
  system = {
    stateVersion = currentStateVersion;                                               # Version of NixOS.
    autoUpgrade = {
      enable = autoUpgrade;                                                      # Enable auto-upgrade.
      allowReboot = false;                                                # Allow reboot.
      channel = currentStateChannel;              # Channel to use for auto-upgrade.
      operation = "switch";                                               # Operation to perform on auto-upgrade.
    };
    activationScripts = {
      chownHome = {
        text = ''
          chown -Rf ${user}:${user} /home/${user}
        '';
      };
    };
  };

}