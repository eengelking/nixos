#!/run/current-system/sw/bin/env bash

USER=$1
OMZ_DATA=$2

git clone "https://github.com/denysdovhan/spaceship-prompt.git" "${OMZ_DATA}/themes/spaceship-prompt" --depth=1 2>/dev/null >/dev/null
ln -s "${OMZ_DATA}/themes/spaceship-prompt/spaceship.zsh-theme" "${OMZ_DATA}/themes/spaceship.zsh-theme" 2>/dev/null >/dev/null

## Copy dotfiles to home
cp dotfiles/.* "${HOME}"
chown -Rf "${USER}:${USER}" "${HOME}"