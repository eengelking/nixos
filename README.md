# NixOS Installation Notes
1. Boot LiveCD
2. Use parted to create the required partitions
```bash
## UEFI Partition
parted /dev/sda -- mklabel gpt
parted /dev/sda -- mkpart ESP fat32 1MiB 10GiB # /boot
parted /dev/sda -- set 3 esp on
## OR
## BIOS Partition
parted /dev/sda -- mklabel msdos
parted /dev/sda -- mkpart primary ext4 1MiB 10GiB # /boot
parted /dev/sda -- set 1 boot on
## ROOT and other partitions
parted /dev/sda -- mkpart primary linux-swap 10GiB 20GiB # swap
parted /dev/sda -- mkpart primary ext4 20GiB 70GiB # /root
parted /dev/sda -- mkpart primary ext4 70GiB 100% # /home
```

3. Format the partitions
```bash
mkfs.fat -F 32 -n boot /dev/sda1
mkswap -L swap /dev/sda2
mkfs.ext4 -L root /dev/sda3
mkfs.ext4 -L home /dev/sda4
```

4. Mount the partitions
```bash
mkdir -p /mnt/boot
mount /dev/disk/by-label/boot /mnt/boot
mount /dev/disk/by-label/root /mnt
swapon /dev/sda2
```

5. Generate the configuration
```bash
nixos-generate-config --root /mnt
```

6. Change directory and edit the boot portion of the configuration
```bash
cd /mnt/etc/nixos
vim configuration.nix
```

> **NOTE:**See the [configuration.nix](configuration.nix) file for an example configuration. You will only need to edit the boot portion of the configuration at this point.

7. Edit the hardware configuration
```bash
## Edit the hardware configuration
vim /etc/nixos/hardware-configuration.nix

## Update the fileSystems section
fileSystems."/" =
  { device = "/dev/disk/by-label/root";
    fsType = "ext4";
  };
```

8. Install NixOS
```bash
## Install NixOS
nixos-install

## After installation, applying changes to the configuration
sudo nixos-rebuild switch
```

9. Reboot
```bash
sudo reboot
```

> **NOTE:**The following sections cover the installation and use of Home Manager. The [configuration.nix](configuration.nix) file is configured for use with Home Manager and contains a `home-manager` section. If Home Manager is not being used, remove the `home-manager` section and Home Manager import, and ignore the following sections.

10. Add the Home-Manager Channel
```bash
## Add a specific release channel
sudo nix-channel --add https://github.com/nix-community/home-manager/archive/release-23.05.tar.gz home-manager
## OR
## Add the master channel
sudo nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager

## Update the channels
sudo nix-channel --update

## Review the channels (globally)
sudo nix-channel --list
```

11. Update the configuration.nix File

> **NOTE:**The [configuration.nix](configuration.nix) file is configured for use with Home Manager and contains a `home-manager` section. If Home Manager is not being used, remove the `home-manager` section and Home Manager import.

```bash
## Edit the configuration.nix file, changing the variables as needed
sudo vim /etc/nixos/configuration.nix

## Test the configuration
sudo nixos-rebuild test

## Rebuild and switch, assuming no errors
sudo nixos-rebuild switch

## Review the Home Manager activation script output
systemctl status "home-manager-$USER.service"
```

11. Updating and upgrading
```bash
## Update the channel
sudo nix-channel --update

## Update the system
sudo nixos-rebuild switch --upgrade

## Update apps installed with nix-env
nix-env -u '*'
```

12. Upgrading NixOS version
```bash
## Add updated channel
sudo nix-channel --add https://nixos.org/channels/nixos-<version> nixos

## Update the channel
sudo nix-channel --update

## Update the configuration.nix file, changing the stateVersion as needed
sudo vim /etc/nixos/configuration.nix

## Update the system
sudo nixos-rebuild switch --upgrade
```
